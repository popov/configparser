/*
 * Copyright (c) 2013, Boris Popov <popov.b@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute
 * this software for any purpose with or without fee
 * is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS
 * ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL,
 * DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH
 * THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <string>
#include <confobjects.hpp>

#ifndef __parser_hpp__
#define __parser_hpp__

namespace config {

////////////////////////////////////////////////
     typedef std::string String;
     typedef String      ErrorString;
     typedef String      FileName;
////////////////////////////////////////////////

////////////////////////////////////////////////
     typedef std::pair <String, Entity*> Pair;
     typedef std::list <Pair>            RootList;
     typedef RootList::const_iterator    RootListConstIt;
     typedef RootList::iterator          RootListIt;
////////////////////////////////////////////////

////////////////////////////////////////////////
     String GetAsString (const RootList&);
////////////////////////////////////////////////

////////////////////////////////////////////////
     class Parser {

     public:
	  Parser (const FileName);
	  ~Parser ();

	  bool Parse ();
	  RootList& Get ();

	  ErrorString GetError () const;

     private:
	  Parser (const Parser&);
	  Parser& operator= (const Parser&);

	  const FileName m_FN;
	  RootList m_RL;

	  ErrorString m_Error;

	  void m_Clean ();
     };
////////////////////////////////////////////////
}
#endif // __parser_hpp__
