/*
 * Copyright (c) 2013, Boris Popov <popov.b@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute
 * this software for any purpose with or without fee
 * is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS
 * ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL,
 * DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH
 * THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <string>
#include <list>
#include <map>

#ifndef __confobjects_hpp__
#define __confobjects_hpp__

namespace config {

////////////////////////////////////////////////
     typedef std::string String;
////////////////////////////////////////////////

////////////////////////////////////////////////
     class Entity {

     public:
	  enum TYPE { UNDEF, SCALAR, LIST, HASH, };
	  enum NEED_CLEAN {
	       
	       NEED_CLEAN_UNDEF,
	       NEED_CLEAN_NO,
	       NEED_CLEAN_YES,
	  };

     public:
	  Entity ();
	  Entity (const TYPE);
	  Entity (const TYPE, const NEED_CLEAN);

	  virtual ~Entity () = 0;

	  TYPE GetType () const;
	  virtual String GetAsString () const = 0;

	  void WillBeClean ();

     private:
	  TYPE type;
	  String GetTypeAsString (const TYPE) const;

     protected:
	  String GetTypeAsStringFirst () const;

     public:
	  NEED_CLEAN m_NeedClean;
     };
////////////////////////////////////////////////

////////////////////////////////////////////////
     class Scalar : public Entity {

     public:
	  enum ScalarType {
	       
	       ST_UNDEF,
	       ST_STRING,
	       ST_INT,
	       ST_DOUBLE,
	       ST_POINTER,
	  };

     public:
	  Scalar ();
	  Scalar (const ScalarType);

	  virtual ~Scalar () = 0;

	  ScalarType GetScalarType () const;
	  virtual String GetAsString () const = 0;

     private:
	  ScalarType m_ST;
	  String GetScalarTypeAsString (const ScalarType) const;

     protected:
	  String GetScalarAsStringFirstPart () const;
     };
////////////////////////////////////////////////

////////////////////////////////////////////////
     class ScalarString : public Scalar {

     public:
	  ScalarString ();
	  ScalarString (const String);

	  virtual ~ScalarString ();

	  virtual String GetAsString () const;

	  String content;
     };
////////////////////////////////////////////////

////////////////////////////////////////////////
     class ScalarInt : public Scalar {

     public:
	  ScalarInt ();
	  ScalarInt (const String);
	  ScalarInt (const int);

	  virtual ~ScalarInt ();

	  virtual String GetAsString () const;

	  int content;
     };
////////////////////////////////////////////////

////////////////////////////////////////////////
     class ScalarDouble : public Scalar {

     public:
	  ScalarDouble ();
	  ScalarDouble (const String);
	  ScalarDouble (const double);

	  virtual ~ScalarDouble ();

	  virtual String GetAsString () const;

	  double content;
     };
////////////////////////////////////////////////

////////////////////////////////////////////////
     class ScalarPointer : public Scalar {

     public:
	  ScalarPointer ();
	  ScalarPointer (Entity*);

	  virtual ~ScalarPointer ();

	  virtual String GetAsString () const;

	  Entity* content;
     };
////////////////////////////////////////////////

////////////////////////////////////////////////
     class List : public Entity {

     public:
	  typedef std::list <Entity*>        EntityList;
	  typedef EntityList::const_iterator EntityListConstIt;
	  typedef EntityList::iterator       EntityListIt;

     public:
	  List ();
	  List (const EntityList&);

	  virtual ~List ();

	  virtual String GetAsString () const;

	  EntityList list;
     };
////////////////////////////////////////////////

////////////////////////////////////////////////
     class Hash : public Entity {

     public:
	  typedef std::map  <String, Entity*> EntityMap;
	  typedef std::pair <String, Entity*> Pair;
	  typedef EntityMap::const_iterator   EntityMapConstIt;
	  typedef EntityMap::iterator         EntityMapIt;

     public:
	  Hash ();
	  Hash (const EntityMap&);

	  virtual ~Hash ();

	  virtual String GetAsString () const;

	  EntityMap map;
     };
////////////////////////////////////////////////
}
#endif // __confobjects_hpp__
