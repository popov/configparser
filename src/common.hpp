/*
 * Copyright (c) 2013, Boris Popov <popov.b@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute
 * this software for any purpose with or without fee
 * is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS
 * ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL,
 * DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH
 * THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <config.h>

#ifndef __common_hpp__
#define __common_hpp__

#define QUOTE(name) #name
#define STR(macro)  QUOTE(macro)

#define SCRIPTDIR_Q    STR (SCRIPTDIR)

#define SCRIPTREADER   SCRIPTDIR/SCRIPTREADERS_NAME
#define SCRIPTREADER_Q STR (SCRIPTREADER)

#define SCRIPTMAKER   SCRIPTDIR/SCRIPTMAKERS_NAME
#define SCRIPTMAKER_Q STR (SCRIPTMAKER)

#endif // __common_hpp__
