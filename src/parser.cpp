/*
 * Copyright (c) 2013, Boris Popov <popov.b@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute
 * this software for any purpose with or without fee
 * is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS
 * ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL,
 * DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH
 * THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <parser.hpp>
#include <common.hpp>

extern "C" {

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
}

////////////////////////////////////////////////
static int make_string       (lua_State*);
static int make_int          (lua_State*);
static int make_double       (lua_State*);
static int make_pointer_list (lua_State*);
static int make_pointer_hash (lua_State*);
static int save              (lua_State*);
static int add_to_list       (lua_State*);
static int add_to_hash       (lua_State*);
////////////////////////////////////////////////

////////////////////////////////////////////////
static int add_to_hash (lua_State* s) {

     void* hash = lua_touserdata (s, 1);
     config::String key ( lua_tostring (s, 2) );
     void* pointer = lua_touserdata (s, 3);

     lua_remove (s, 1);
     lua_remove (s, 1);
     lua_remove (s, 1);

     config::Hash* hs = (config::Hash*) hash;
     config::Pair p (key, (config::Entity*) pointer);
     hs->map.insert (p);

     return 0;
}
////////////////////////////////////////////////

////////////////////////////////////////////////
static int add_to_list (lua_State* s) {

     void* list = lua_touserdata (s, 1);
     void* pointer = lua_touserdata (s, 2);

     lua_remove (s, 1);
     lua_remove (s, 1);

     config::List* pl = (config::List*) list;
     pl->list.push_back ( (config::Entity*) pointer );

     return 0;
}
////////////////////////////////////////////////

////////////////////////////////////////////////
static int make_pointer_list (lua_State* s) {

     config::List* li = new config::List;
     li->WillBeClean ();
     lua_pushlightuserdata (s, (void*) li);
     return 1;
}
////////////////////////////////////////////////

////////////////////////////////////////////////
static int make_pointer_hash (lua_State* s) {

     config::Hash* hs = new config::Hash;
     hs->WillBeClean ();
     lua_pushlightuserdata (s, (void*) hs);
     return 1;
}
////////////////////////////////////////////////

////////////////////////////////////////////////
static int save (lua_State* s) {

     void* rootlist = lua_touserdata (s, 1);
     config::String name (lua_tostring (s, 2));
     void* ent = lua_touserdata (s, 3);

     lua_remove (s, 1);
     lua_remove (s, 1);
     lua_remove (s, 1);

     config::Pair p (name, (config::Entity*) ent);
     config::RootList* rl = (config::RootList*) rootlist;
     rl->push_back (p);

     return 0;
}
////////////////////////////////////////////////

////////////////////////////////////////////////
static int make_int (lua_State* s) {

     lua_Integer i = lua_tointeger (s, 1);
     lua_remove (s, 1);
     config::ScalarInt* si = new config::ScalarInt (i);
     si->WillBeClean ();
     lua_pushlightuserdata (s, (void*) si);
     return 1;
}
////////////////////////////////////////////////

////////////////////////////////////////////////
static int make_double (lua_State* s) {

     lua_Number d = lua_tonumber (s, 1);
     lua_remove (s, 1);
     config::ScalarDouble* sd = new config::ScalarDouble (d);
     sd->WillBeClean ();
     lua_pushlightuserdata (s, (void*) sd);
     return 1;
}
////////////////////////////////////////////////

////////////////////////////////////////////////
static int make_string (lua_State* s) {

     config::String ms ( lua_tostring (s, 1) );
     lua_remove (s, 1);
     config::ScalarString* ss = new config::ScalarString (ms);
     ss->WillBeClean ();
     lua_pushlightuserdata (s, (void*) ss);
     return 1;
}
////////////////////////////////////////////////

////////////////////////////////////////////////
config::Parser::Parser (const FileName fn):

     m_FN (fn),
     m_Error ("")
{
     return;
}

config::Parser::~Parser () {

     m_Clean ();
     return;
}

void config::Parser::m_Clean () {

     if ( m_RL.size () == 0 )  return;

     RootListConstIt it = m_RL.begin ();
     RootListConstIt end = m_RL.end ();

     while (it != end) {

	  if ( (*it).second != NULL) {

	       delete (*it).second;
	  }
	  it++;
     }
     return;
}

bool config::Parser::Parse () {

     const bool Success = true;
     const bool Fault = false;

     m_Clean ();
     m_Error = "";

     lua_State* L = luaL_newstate ();
     if (L == NULL) {

	  m_Error = "Error creating Lua context.";
	  return Fault;
     }
     luaL_openlibs (L);

     if ( luaL_dofile (L, SCRIPTMAKER_Q) != LUA_OK ) {

	  m_Error = "Error opening " + String (SCRIPTMAKER_Q);
	  return Fault;
     }

     lua_getglobal (L, "Init"); 
     lua_pushstring (L, SCRIPTDIR_Q);
     lua_pushlightuserdata (L, (void*) (&m_RL));
     lua_call (L, 2, 0);

     lua_register (L, "make_int", make_int);  
     lua_register (L, "make_double", make_double);  
     lua_register (L, "make_string", make_string);  
     lua_register (L, "make_pointer_list", make_pointer_list);  
     lua_register (L, "make_pointer_hash", make_pointer_hash);  
     lua_register (L, "add_to_list", add_to_list);  
     lua_register (L, "add_to_hash", add_to_hash);  
     lua_register (L, "save", save);  

     lua_getglobal (L, "Main"); 
     lua_pushstring (L, m_FN.c_str ());
     lua_call (L, 1, 1);
     
     int result = lua_toboolean (L, 1);

     lua_remove (L, 1);
     lua_close (L);

     if (result)  return Success;
     return Fault;
}

config::RootList& config::Parser::Get () {

     return m_RL;
}

config::ErrorString config::Parser::GetError () const {

     return m_Error;
}
////////////////////////////////////////////////

////////////////////////////////////////////////
config::String config::GetAsString (const RootList& rl) {

     String result ("");

     RootListConstIt it = rl.begin ();
     RootListConstIt end = rl.end ();

     bool first = true;
     while (it != end) {

	  if (! first)  result += "\n";
	  result += (*it).first;
	  result += "_";
	  result += ((*it).second)->GetAsString ();

	  first = false;
	  it++;
     }
     return result;
}
////////////////////////////////////////////////
