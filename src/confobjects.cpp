/*
 * Copyright (c) 2013, Boris Popov <popov.b@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute
 * this software for any purpose with or without fee
 * is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS
 * ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL,
 * DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH
 * THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <confobjects.hpp>
#include <sstream>

////////////////////////////////////////////////
config::Entity::Entity ():

     type (UNDEF),
     m_NeedClean (NEED_CLEAN_NO)
{
     return;
}

config::Entity::Entity (const TYPE t):

     type (t),
     m_NeedClean (NEED_CLEAN_NO)
{
     return;
}

config::Entity::Entity (const TYPE t, const NEED_CLEAN nc):

     type (t),
     m_NeedClean (nc)
{
     return;
}

void config::Entity::WillBeClean () {

     m_NeedClean = NEED_CLEAN_YES;
     return;
}

config::Entity::~Entity () {

     return;
}

config::Entity::TYPE config::Entity::GetType () const {

     return type;
}

config::String config::Entity::GetTypeAsString
(const TYPE t) const {

     String result ("");
     switch (t) {

     case UNDEF:
	  result = "UNDEF";
	  break;

     case SCALAR:
	  result = "SCALAR";
	  break;

     case LIST:
	  result = "LIST";
	  break;

     case HASH:
	  result = "HASH";
	  break;
     }
     return result;
}

config::String config::Entity::GetTypeAsStringFirst () const {

     return String ( "[" + GetTypeAsString (type) + "]" );
}
////////////////////////////////////////////////

////////////////////////////////////////////////
config::Scalar::Scalar ():

     Entity (SCALAR),
     m_ST (ST_UNDEF)
{
     return;
}

config::Scalar::Scalar (const ScalarType st):

     Entity (SCALAR),
     m_ST (st)
{
     return;
}

config::Scalar::~Scalar () {

     return;
}

config::Scalar::ScalarType config::Scalar::GetScalarType () const {

     return m_ST;
}

config::String config::Scalar::GetScalarTypeAsString
(const ScalarType t) const {

     String result ("");
     switch (t) {

     case ST_UNDEF:
	  result = "ST_UNDEF";
	  break;

     case ST_STRING:
	  result = "ST_STRING";
	  break;

     case ST_INT:
	  result = "ST_INT";
	  break;

     case ST_DOUBLE:
	  result = "ST_DOUBLE";
	  break;

     case ST_POINTER:
	  result = "ST_POINTER";
	  break;
     }
     return result;
}

config::String config::Scalar::GetScalarAsStringFirstPart () const {

     return String (GetTypeAsStringFirst () +
		    "-["
		    +GetScalarTypeAsString (m_ST) +
		    "]");
}
////////////////////////////////////////////////

////////////////////////////////////////////////
config::ScalarString::ScalarString ():

     Scalar (ST_STRING),
     content ("")
{
     return;
}

config::ScalarString::ScalarString (const String str):

     Scalar (ST_STRING),
     content (str)
{
     return;
}

config::ScalarString::~ScalarString () {

     return;
}

config::String config::ScalarString::GetAsString () const {

     return String (GetScalarAsStringFirstPart () +
		    "-[" +
		    content +
		    "]");
}
////////////////////////////////////////////////

////////////////////////////////////////////////
config::ScalarInt::ScalarInt ():

     Scalar (ST_INT),
     content (0)
{
     return;
}

config::ScalarInt::ScalarInt (const String str):

     Scalar (ST_INT),
     content ( std::stoi (str) )
{
     return;
}

config::ScalarInt::ScalarInt (const int n):

     Scalar (ST_INT),
     content (n)
{
     return;
}

config::ScalarInt::~ScalarInt () {

     return;
}

config::String config::ScalarInt::GetAsString () const {

     return String (GetScalarAsStringFirstPart () +
		    "-[" +
		    std::to_string (content) +
		    "]");
}
////////////////////////////////////////////////

////////////////////////////////////////////////
config::ScalarDouble::ScalarDouble ():

     Scalar (ST_DOUBLE),
     content (0.0)
{
     return;
}

config::ScalarDouble::ScalarDouble (const String str):

     Scalar (ST_DOUBLE),
     content ( std::stod (str) )
{
     return;
}

config::ScalarDouble::ScalarDouble (const double n):

     Scalar (ST_DOUBLE),
     content (n)
{
     return;
}

config::ScalarDouble::~ScalarDouble () {

     return;
}

config::String config::ScalarDouble::GetAsString () const {

     return String (GetScalarAsStringFirstPart () +
		    "-[" +
		    std::to_string (content) +
		    "]");
}
////////////////////////////////////////////////

////////////////////////////////////////////////
config::ScalarPointer::ScalarPointer ():

     Scalar (ST_POINTER),
     content (NULL)
{
     return;
}

config::ScalarPointer::ScalarPointer (Entity* e):

     Scalar (ST_POINTER),
     content (e)
{
     return;
}

config::ScalarPointer::~ScalarPointer () {

     return;
}

config::String config::ScalarPointer::GetAsString () const {

     std::stringstream ss;
     ss << (void*) content;
     return String (GetScalarAsStringFirstPart () +
		    "-[" +
		    ss.str () +
		    "]");
}
////////////////////////////////////////////////

////////////////////////////////////////////////
config::List::List ():

     Entity (LIST)
{
     list.clear ();
     return;
}

config::List::List (const EntityList& el):

     Entity (LIST),
     list (el)
{
     return;
}

config::List::~List () {

     EntityListConstIt it = list.begin ();
     EntityListConstIt end = list.end ();

     while (it != end) {

	  if ( (*it) != NULL ) {

	       if ( (*it)->m_NeedClean == NEED_CLEAN_YES) {

		    delete (*it);
	       }
	  }
	  it++;
     }
     return;
}

config::String config::List::GetAsString () const {

     String result ( GetTypeAsStringFirst () );
     if (list.size () == 0)  return result;
     EntityListConstIt it = list.begin ();
     EntityListConstIt end = list.end ();
     result += "-[";
     bool first = true;
     while (it != end) {

	  if (! first)  result += ",";
	  result += (*it)->GetAsString ();
	  first = false;
	  it++;
     }
     result += "]";
     return result;
}
////////////////////////////////////////////////

////////////////////////////////////////////////
config::Hash::Hash ():

     Entity (HASH)
{
     map.clear ();
     return;
}

config::Hash::Hash (const EntityMap& m):

     Entity (HASH),
     map (m)
{
     return;
}

config::Hash::~Hash () {

     EntityMapConstIt it = map.begin ();
     EntityMapConstIt end = map.end ();

     while (it != end) {

	  if ( (*it).second != NULL ) {

	       if ( (*it).second->m_NeedClean == NEED_CLEAN_YES) {

		    delete ( (*it).second );
	       }
	  }
	  it++;
     }
     return;
}

config::String config::Hash::GetAsString () const {

     String result ( GetTypeAsStringFirst () );
     if (map.size () == 0)  return result;
     EntityMapConstIt it = map.begin ();
     EntityMapConstIt end = map.end ();
     result += "-[";
     bool first = true;
     while (it != end) {

	  if (! first)  result += ",";
	  result += (*it).first;
	  result += "=>";
	  result += (*it).second->GetAsString ();
	  first = false;
	  it++;
     }
     result += "]";
     return result;
}
////////////////////////////////////////////////
