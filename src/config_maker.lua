-- Copyright (c) 2013, Boris Popov <popov.b@gmail.com>
--
-- Permission to use, copy, modify, and/or distribute
-- this software for any purpose with or without fee
-- is hereby granted, provided that the above copyright
-- notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS
-- ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
-- IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL,
-- DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
-- WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
-- TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH
-- THE USE OR PERFORMANCE OF THIS SOFTWARE.

local RootList

function LoadRootList ()

   return RootList
end

function Init (path, rl)

   package.path = path.."/?.lua;"..package.path
   RootList = rl
   require ("config_reader")
   return
end

local function Save (name, p)

   save (RootList, name, p)
   return
end

local make_list, make_hash, MakePointer

make_hash = function (hash)

   local hs = make_pointer_hash ()

   for k, v in pairs (hash) do

      local pointer = MakePointer (v)

      if (pointer ~= nil) then

	 add_to_hash (hs, k, pointer)
      end
   end

   return hs
end

local function MakeHash (i, name, hash)

   local result = make_hash (hash)
   Save (name, result)
   return
end

MakePointer = function (val)

   local pointer = nil

   if (val.TYPE == "SCALAR") then

      if (val.SUBTYPE == "INT") then

	 pointer = make_int (val.VALUE)

      elseif (val.SUBTYPE == "STRING") then

	 pointer = make_string (val.VALUE)

      elseif (val.SUBTYPE == "DOUBLE") then

	 pointer = make_double (val.VALUE)

      end

   elseif (val.TYPE == "LIST") then

      pointer = make_list (val.VALUE)
      
   elseif (val.TYPE == "HASH") then

      pointer = make_hash (val.VALUE)

   end

   return pointer
end

make_list = function (list)

   local lst = make_pointer_list ()

   for i, v in ipairs (list) do

      local pointer = MakePointer (v)

      if (pointer ~= nil) then

	 add_to_list (lst, pointer)
      end
   end

   return lst
end

local function MakeList (i, name, list)

   local result = make_list (list)
   Save (name, result)
   return
end

local function MakeString (i, name, v)

   local result = make_string (v)
   Save (name, result)
   return
end

local function MakeInt (i, name, v)

   local result = make_int (v)
   Save (name, result)
   return
end

local function MakeDouble (i, name, v)

   local result = make_double (v)
   Save (name, result)
   return
end

local function MakeScalar (i, name, v)

   if (v.SUBTYPE == "STRING") then

      MakeString (i, name, v.VALUE)

   elseif (v.SUBTYPE == "INT") then

      MakeInt (i, name, v.VALUE)

   elseif (v.SUBTYPE == "DOUBLE") then

      MakeDouble (i, name, v.VALUE)

   end
   return
end

function Main (filename)

   local events = {}
   if ( not Read (filename, events) ) then

      return false
   end

   for i, v in ipairs (events) do

      local name = v.NAME

      --print (i.." "..tostring (v))

      if (v.TYPE == "SCALAR") then

	 MakeScalar (i, name, v)
     
      elseif (v.TYPE == "LIST") then

	 MakeList (i, name, v.VALUE)
      
      elseif (v.TYPE == "HASH") then

	 MakeHash (i, name, v.VALUE)

      end
   end
   return true
end
