-- Copyright (c) 2013, Boris Popov <popov.b@gmail.com>
--
-- Permission to use, copy, modify, and/or distribute
-- this software for any purpose with or without fee
-- is hereby granted, provided that the above copyright
-- notice and this permission notice appear in all copies.
--
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS
-- ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
-- IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL,
-- DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
-- WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
-- WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
-- TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH
-- THE USE OR PERFORMANCE OF THIS SOFTWARE.

local EVENTS = {}
local meta_ent = {

   __tostring =
      function (table)

	 local result = table.TYPE
	 result = result.." "
	 if (table.TYPE == "SCALAR") then

	    result = result..table.SUBTYPE
	    result = result.." "
	 end

	 result = result..table.NAME.." "
	 result = result..tostring (table.VALUE)

	 return result
      end
}

local meta_list = {

   __tostring =
      function (list)

	 local result = "["
	 local first = true
	 for i, v in ipairs (list) do

	    if (not first) then

	       result = result.." "
	    end

	    result = result..v.TYPE.."-"
	    if ( v.SUBTYPE ~= nil ) then

	       result = result..v.SUBTYPE.."-"
	    end

	    result = result..tostring (v.VALUE)
	    first = false
	 end
	 result = result.."]"
	 return result
      end
}

local meta_hash = {

   __tostring =
      function (hash)
	 
   	 local result = "["
   	 local first = true
   	 for i, v in pairs (hash) do
	    
	    if (not first) then
	       
	       result = result.." "
	    end
	    
	    result = result..i.." => "
	    result = result..v.TYPE.."-"

	    if ( v.SUBTYPE ~= nil ) then
	       
	       result = result..v.SUBTYPE.."-"
	    end

	    result = result..tostring (v.VALUE)
	    first = false
   	 end
   	 result = result.."]"
	 return result
      end
}

local function RemoveComment (line)

   local pos = string.find (line, "#+");
   if (pos) then
      line = string.sub (line, 1, pos - 1)
   end
   return line
end

local function RemoveFirstSpaces (line)

   local f = string.match (line, "^%s+");
   if (f) then
      line = string.sub (line, string.len (f) + 1)
   end
   return line
end

local function RemoveSpace (line)

   line = string.reverse (line)
   line = RemoveFirstSpaces (line)
   line = string.reverse (line)
   line = RemoveFirstSpaces (line)
   return line
end

local function IsItComment (line)

   local comment = string.match (line, "^%s*#+");
   if (comment) then
      return true
   else
      return false
   end
end

local function IsIt (line, symbol)

   s = string.byte (symbol)
   first = string.byte (line, 1)

   if (s == first) then
      return true
   end

   return false
end

local function IsItScalar (line)

   return IsIt (line, '$')
end

local function GetFirst (line, shift)
   
   local value
   local pos = string.find (line, "=", 1);
   if (pos) then

      value = string.sub (line, shift, pos - 1)
      value = RemoveSpace (value)
   end
   return value
end

local function GetLast (line, shift)
   
   local value
   local pos = string.find (line, "=", 1);
   if (pos) then
      
      local l2 = string.sub (line, pos + 1)
      value = RemoveSpace (l2)
   end
   return value
end

local function IsItString (value)

   quote = string.byte ('"')
   first = string.byte (value, 1)
   last = string.byte (value, -1)

   if (first == quote) then
      if (last == quote) then
	 return true
      end
   end

   return false
end

local function RemoveQuote (value)

   return string.sub (value, 2, -2)
end

local function GetNumberSubtype (value)

   local subtype = nil

   if (string.find (value, "[.]") ~= nil) then

      subtype = "DOUBLE"

   else

      subtype = "INT"
   end

   return subtype
end

local function IsValueScalar (value)

   dollar = string.byte ('$')
   first = string.byte (value, 1)

   if (dollar == first) then
      return true
   end

   return false
end

local function IsScalarFrom (value, one, two)

   local pos1 = string.find (value, one)
   local pos2 = string.find (value, two)

   if (pos1 ~= nil) then
      if (pos2 ~= nil) then
	 if (pos2 > pos1) then

	    return true
	 end
      end
   end
	 
   return false
end

local function IsScalarFromList (value)

   return IsScalarFrom (value, '[[]', '[]]')
end

local function IsScalarFromHash (value)

   return IsScalarFrom (value, '[{]', '[}]')
end

local function IsScalarFromListOrHash (value)

   if (IsScalarFromList (value)) then

      return true
   end

   if (IsScalarFromHash (value)) then

      return true
   end

   return false
end

local function GetNameFrom (value, limit)
   
   local result = nil
   local pos = string.find (value, limit)

   if (pos ~= nil) then

      result = string.sub (value, 1, pos - 1)
      result = RemoveSpace (result)
   end

   return result
end

local function GetNameFromList (value)

   return GetNameFrom (value, "[[]")
end

local function GetNameFromHash (value)

   return GetNameFrom (value, "[{]")
end

local function GetIndexFromList (value)

   local result = nil

   local pos1 = string.find (value, "[[]")
   local pos2 = string.find (value, "[]]")

   if (pos1 ~= nil) then
      if (pos2 ~= nil) then
	 if (pos2 > pos1) then

	    result = string.sub (value, pos1 + 1, pos2 - 1)
	    result = RemoveSpace (result)
	 end
      end
   end

   return result
end

local function ItHas (str, delimiter)

   local first = string.byte (str, 1)
   local last = string.byte (str, -1)
   local my = string.byte (delimiter)

   if (first == my) then
      if (last == my) then

	 return true
      end
   end

   return false
end

local function ItHasApos (str)

   return ItHas (str, "'")
end

local function ItHasQuot (str)

   return ItHas (str, "\"")
end

local function RemoveFirstAndLast (str)

   return string.sub (str, 2, -2)
end

local function RemoveAposQuot (str)

   result = str

   if (ItHasApos (str)) then

      result = RemoveFirstAndLast (str)
   end

   if (ItHasQuot (str)) then

      result = RemoveFirstAndLast (str)
   end
   return result
end

local function GetKeyFromHash (value)

   local result = nil

   local pos1 = string.find (value, "[{]")
   local pos2 = string.find (value, "[}]")

   if (pos1 ~= nil) then
      if (pos2 ~= nil) then
	 if (pos2 > pos1) then

	    result = string.sub (value, pos1 + 1, pos2 - 1)
	    result = RemoveSpace (result)
	    result = RemoveAposQuot (result)
	 end
      end
   end

   return result
end

local function GetFrom (value)

   local result = nil
   local type = nil
   local name = nil
   local index = nil
   local key = nil

   if (IsScalarFromList (value)) then

      type = "LIST"
      name = GetNameFromList (value)
      index = GetIndexFromList (value)

   elseif (IsScalarFromHash (value)) then

      type = "HASH"
      name = GetNameFromHash (value)
      key = GetKeyFromHash (value)

   else

      return result
   end

   if (type ~= nil) then
      if (name ~= nil) then
	 if (index ~= nil) then

	    result = {

	       TYPE = type,
	       NAME = name,
	       INDEX = index,
	    }
	    
	 elseif (key ~= nil) then

	    result = {

	       TYPE = type,
	       NAME = name,
	       KEY = key,
	    }

	 end
      end
   end

   return result
end

local function GetPrevSubtypeValue (value, type)

   local from = nil
   if (IsScalarFromListOrHash (value)) then

      from = GetFrom (value)
   end

   if (#EVENTS == 0) then

      return nil, nil
   end
   
   for i = #EVENTS, 1, -1 do

      local ev = EVENTS[i]

      if (from ~= nil) then

	 if (ev.NAME == from.NAME) then
	    if (ev.TYPE == from.TYPE) then
	       
	       if (from.INDEX ~= nil) then

		  return ev.VALUE[from.INDEX + 1].SUBTYPE,
		  ev.VALUE[from.INDEX + 1].VALUE
		  
	       elseif (from.KEY ~= nil) then

		  return ev.VALUE[from.KEY].SUBTYPE,
		  ev.VALUE[from.KEY].VALUE

	       end
	    end
	 end
	    
      elseif (ev.TYPE == type) then
	 if (ev.NAME == value) then

	    return ev.SUBTYPE, ev.VALUE
	 end
      end
   end
   return nil, nil
end

local function RemoveDollar (value)

   return string.sub (value, 2)
end

local function ProcessScalar (line)

   local _TYPE = "SCALAR"
   local linea = line;
   local KEY = GetFirst (linea, 2);

   local linev = line;
   local value = GetLast (linev, 1);

   local _SUBTYPE = nil
   if ( IsItString (value) ) then

      _SUBTYPE = "STRING"
      value = RemoveQuote (value)

   elseif ( IsValueScalar (value) ) then

      _SUBTYPE, value =
	 GetPrevSubtypeValue (RemoveDollar (value), _TYPE)
      if (value == nil) then

	 _SUBTYPE = "STRING"
	 value = ""
      end

   else
      
      _SUBTYPE = GetNumberSubtype (value)
   end

   local ent = {

      TYPE    = _TYPE,
      SUBTYPE = _SUBTYPE,
      NAME    = KEY,
      VALUE   = value
   }
   setmetatable (ent, meta_ent) 

   return ent
end

local function IsItList (line)

   return IsIt (line, '@')
end

-- ************************************************* --
local Extractor = {}
local Extractor_mt = { __index = Extractor } 
function Extractor:new (str)

   return setmetatable ( { String = str }, Extractor_mt )
end

function Extractor:next ()

   if (string.len (self.String) == 0) then
      
      return nil
   end

   local pos = string.find (self.String, "[,]")
   if (pos == nil) then

      local result = RemoveSpace (self.String)
      self.String = ""
      return result
   end

   local result = string.sub (self.String, 1, pos - 1)
   result = RemoveSpace (result)

   self.String = string.sub (self.String, pos + 1)
   return result
end
-- ************************************************* --

local function GetTypeSubtypeValue (lexem)

   local type = nil
   local subtype = nil
   local value = nil

   local dollar = string.byte ('$')
   local dog = string.byte ('@')
   local proc = string.byte ('%')

   local first = string.byte (lexem, 1)

   if (dollar == first) then

      type = "SCALAR"
      subtype, value =
	 GetPrevSubtypeValue (RemoveDollar (lexem), type)

      if (value == nil) then

	 subtype = "STRING"
	 value = ""
      end

   elseif (dog == first) then

      type = "LIST"
      subtype, value =
	 GetPrevSubtypeValue (RemoveDollar (lexem), type)

      if (value == nil) then

	 type = "SCALAR"
	 subtype = "STRING"
	 value = ""
      end

   elseif (proc == first) then

      type = "HASH"
      subtype, value =
	 GetPrevSubtypeValue (RemoveDollar (lexem), type)

      if (value == nil) then

	 type = "SCALAR"
	 subtype = "STRING"
	 value = ""
      end

   else

      if ( IsItString (lexem) ) then

	 type = "SCALAR"
	 subtype = "STRING"
	 value = RemoveQuote (lexem)

      else

	 type = "SCALAR"
	 subtype = GetNumberSubtype (lexem)
	 value = lexem

      end
   end
   return type, subtype, value
end

local function GetListValues (line)

   local values = {}
   setmetatable (values, meta_list) 
   local lbr = string.find (line, "[(]")
   local rbr = string.find (line, "[)]")

   if (lbr ~= nil) then
      if (rbr ~= nil) then

	 local nline = string.sub (line,
				   lbr + 1,
				   rbr - 1)

	 nline = RemoveSpace (nline)
	 local e = Extractor:new (nline)
	 
	 local lexem = e:next ()
	 while lexem do

	    local type
	    local subtype
	    local value
	    type, subtype, value = GetTypeSubtypeValue (lexem)

	    if (type ~= nil) then
	       if (value ~= nil) then

		  local ent = {

		     TYPE = type,
		     SUBTYPE = subtype,
		     VALUE = value
		  }

		  table.insert (values, ent)
	       end
	    end

	    lexem = e:next ()
	 end
      end
   end
   return values
end

local function ProcessList (line)

   local KEY = GetFirst (line, 2);
   local values = GetListValues (line)

   local ent = {

      TYPE    = "LIST",
      NAME    = KEY,
      VALUE   = values
   }
   setmetatable (ent, meta_ent) 
   return ent
end

local function IsItHash (line)

   return IsIt (line, '%')
end

local function GetRightTail (line)

   local pos = string.find (line, "[=]")
   local result = ""
   result = string.sub (line, pos + 1)
   return result
end

local function GetKey (lexem)
   
   local result = ""
   local pos = string.find (lexem, "=>")
   result = string.sub (lexem, 1, pos - 1)
   result = RemoveSpace (result)
   return RemoveAposQuot (result)
end

local function GetValue (lexem)
   
   local result = ""
   local pos = string.find (lexem, "=>")
   result = string.sub (lexem, pos + 2)
   return RemoveSpace (result)
end

local function GetHashValues (line)

   local values = {}
   setmetatable (values, meta_hash) 
   local newline = GetRightTail (line)

   local lbr = string.find (line, "[(]")
   local rbr = string.find (line, "[)]")

   if (lbr ~= nil) then
      if (rbr ~= nil) then
	 
   	 local nline =
	    string.sub (line, lbr + 1, rbr - 1)
   	 nline = RemoveSpace (nline)
   	 local e = Extractor:new (nline)
   	 
   	 local lexem = e:next ()
   	 while lexem do

	    local _key = GetKey (lexem)
	    local _value = GetValue (lexem)
	    local type = nil
	    local subtype = nil
	    local value = nil

	    type, subtype, value = GetTypeSubtypeValue (_value)
	    
	    if (type ~= nil) then
	       if (value ~= nil) then
		  
		  local ent_val = {
		     
		     TYPE = type,
		     SUBTYPE = subtype,
		     VALUE = value
		  }

		  values[_key] = ent_val
	       end
	    end

   	    lexem = e:next ()
   	 end
      end
   end
   return values
end

local function ProcessHash (line)

   local KEY = GetFirst (line, 2);
   local values = GetHashValues (line)

   local ent = {

      TYPE    = "HASH",
      NAME    = KEY,
      VALUE   = values
   }
   setmetatable (ent, meta_ent) 
   return ent
end

local function HasItContinueSign (mline)

   local line = RemoveSpace (mline)
   local slash = string.byte ('\\')
   local last = string.byte (line, -1)
   if (last == slash) then

      return true
   end

   return false
end

local function RemoveContinueSign (mline)

   local line = RemoveSpace (mline)
   local new = string.sub (line, 1, -2)
   new = " "..new.." "
   return new
end

function Read (filename, events)

   EVENTS = {}
   local fh = nil
   fh = io.open (filename)

   if (fh == nil) then
      return false
   end

   number = 0
   local line = ""
   for mline in fh:lines ()
   do

      if ( HasItContinueSign (mline) ) then

	 line = line..RemoveContinueSign (mline)
	 goto continue

      else

	 line = line..mline
      end

      number = number + 1
      if (not IsItComment (line)) then 

	 line = RemoveComment (line)
	 line = RemoveSpace (line)

	 if ( IsItScalar (line) ) then

	    local sct = ProcessScalar (line)
	    table.insert (events, sct)
	    table.insert (EVENTS, sct)
	    line = ""

	 elseif ( IsItList (line) ) then

	    local l = ProcessList (line)
	    table.insert (events, l)
	    table.insert (EVENTS, l)
	    line = ""

	 elseif ( IsItHash (line) ) then

	    local h = ProcessHash (line)
	    table.insert (events, h)
	    table.insert (EVENTS, h)
	    line = ""

	 else
	    -- ??? TODO ERROR ???
	 end

      else

	 --this is comment
	 line = ""
      end

      ::continue::
   end

   io.close (fh)
   return true
end
