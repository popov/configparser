#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>
#include <iostream>

#include <confobjects.hpp>
#include <parser.hpp>

////////////////////////////////////////////////
class Test_Cobjects: public CppUnit::TestFixture {

     CPPUNIT_TEST_SUITE (Test_Cobjects);
     CPPUNIT_TEST (Test_Cobjects_01);
     CPPUNIT_TEST (Test_Cobjects_02);
     CPPUNIT_TEST_SUITE_END ();

public:

     void setUp () {

	  return;
     }
     
     void tearDown () {

	  return;
     }          
	
     void Test_Cobjects_01 () {

	  config::ScalarString  CS ("one");
	  config::ScalarInt     CI (2);
	  config::ScalarDouble  SD (3.3);
	  config::ScalarPointer SP (&CS);
	  
	  config::List::EntityList EL;
	  EL.push_back (&CS);
	  EL.push_back (&CI);
	  EL.push_back (&SD);
	  EL.push_back (&SP);

	  config::List L (EL);
	  std::cout << std::endl << L.GetAsString () << std::endl;
	  CPPUNIT_ASSERT (true);
	  return;
     }

     void Test_Cobjects_02 () {

	  config::ScalarString  CS ("one");
	  config::ScalarInt     CI (2);
	  config::ScalarDouble  SD (3.3);
	  config::ScalarPointer SP (&CS);
	  
	  config::List::EntityList EL;
	  EL.push_back (&CS);
	  EL.push_back (&CI);
	  EL.push_back (&SD);
	  EL.push_back (&SP);
	  config::List L (EL);


	  config::Hash::EntityMap EM;
	  config::Hash::Pair p1 ("one", &L);
	  config::Hash::Pair p2 ("two", &L);
	  EM.insert (p1);
	  EM.insert (p2);
	  config::Hash H (EM);

	  std::cout << std::endl << H.GetAsString () << std::endl;
	  CPPUNIT_ASSERT (true);
	  return;
     }
};
////////////////////////////////////////////////

////////////////////////////////////////////////
class Test_Parser: public CppUnit::TestFixture {

     CPPUNIT_TEST_SUITE (Test_Parser);
     CPPUNIT_TEST (Test_Parser_01);
     CPPUNIT_TEST_SUITE_END ();

public:

     void setUp () {

	  return;
     }
     
     void tearDown () {

	  return;
     }          
	
     void Test_Parser_01 () {

	  config::Parser P ("test.cfg");
	  if (! P.Parse () ) {

	       std::cerr << std::endl;
	       std::cerr << P.GetError ();
	       std::cerr << std::endl;
	       CPPUNIT_ASSERT (false);
	       return;
	  }

	  config::RootList& RL = P.Get ();

	  std::string example ="\
NewbraiN_[SCALAR]-[ST_DOUBLE]-[333.330000]\n\
a_[SCALAR]-[ST_INT]-[66]\n\
b_[SCALAR]-[ST_INT]-[77]\n\
xi_[SCALAR]-[ST_INT]-[0]\n\
y_[SCALAR]-[ST_DOUBLE]-[0.500000]\n\
zog_[SCALAR]-[ST_STRING]-[string]\n\
az_[SCALAR]-[ST_INT]-[66]\n\
art_[SCALAR]-[ST_INT]-[66]\n\
bart_[SCALAR]-[ST_STRING]-[]\n\
list_1_[LIST]-[[SCALAR]-[ST_INT]-[1],[SCALAR]-[ST_INT]-[2],[SCALAR]-[ST_INT]-[3]]\n\
list_2_[LIST]-[[SCALAR]-[ST_INT]-[66],[SCALAR]-[ST_INT]-[2],[SCALAR]-[ST_INT]-[3]]\n\
list3_[LIST]-[[SCALAR]-[ST_DOUBLE]-[0.333000],[SCALAR]-[ST_STRING]-[ones],[SCALAR]-[ST_STRING]-[string],[SCALAR]-[ST_INT]-[1],[SCALAR]-[ST_DOUBLE]-[0.500000]]\n\
list4_[LIST]-[[SCALAR]-[ST_INT]-[1],[LIST]-[[SCALAR]-[ST_INT]-[1],[SCALAR]-[ST_INT]-[2],[SCALAR]-[ST_INT]-[3]],[SCALAR]-[ST_INT]-[2]]\n\
hash_1_[HASH]-[arg1=>[SCALAR]-[ST_INT]-[1],arg2=>[SCALAR]-[ST_INT]-[2]]\n\
hash_2_[HASH]-[arg0=>[SCALAR]-[ST_DOUBLE]-[0.500000],arg1=>[SCALAR]-[ST_INT]-[1],arg2=>[SCALAR]-[ST_INT]-[2],arg3=>[SCALAR]-[ST_INT]-[66]]\n\
hash_3_[HASH]-[arg1=>[SCALAR]-[ST_STRING]-[one],arg2=>[SCALAR]-[ST_STRING]-[two],arg3=>[SCALAR]-[ST_STRING]-[string]]\n\
hash_4_[HASH]-[arg1=>[SCALAR]-[ST_STRING]-[one],arg2=>[SCALAR]-[ST_STRING]-[two],arg3=>[LIST]-[[SCALAR]-[ST_INT]-[1],[SCALAR]-[ST_INT]-[2],[SCALAR]-[ST_INT]-[3]]]\n\
list_5_[LIST]-[[SCALAR]-[ST_INT]-[1],[SCALAR]-[ST_INT]-[2],[SCALAR]-[ST_INT]-[3],[HASH]-[arg1=>[SCALAR]-[ST_INT]-[1],arg2=>[SCALAR]-[ST_INT]-[2]]]\n\
hash_5_[HASH]-[key_1=>[HASH]-[arg1=>[SCALAR]-[ST_INT]-[1],arg2=>[SCALAR]-[ST_INT]-[2]],key_2=>[HASH]-[arg1=>[SCALAR]-[ST_INT]-[1],arg2=>[SCALAR]-[ST_INT]-[2]]]\n\
homer_[SCALAR]-[ST_INT]-[22]\n\
newbrain_[SCALAR]-[ST_DOUBLE]-[133.130000]\n\
nemo_1_[SCALAR]-[ST_INT]-[66]\n\
nemo_2_[SCALAR]-[ST_STRING]-[one]\n\
nemo_3_[SCALAR]-[ST_STRING]-[two]\n\
nemo_4_[SCALAR]-[ST_STRING]-[two]\n\
hash_6_[HASH]-[key 1=>[SCALAR]-[ST_STRING]-[one]]\n\
hash_7_[HASH]-[key 1=>[SCALAR]-[ST_STRING]-[one]]\n\
list_6_[LIST]-[[SCALAR]-[ST_INT]-[1],[SCALAR]-[ST_INT]-[2],[SCALAR]-[ST_INT]-[3],[SCALAR]-[ST_INT]-[66]]\n\
hash_8_[HASH]-[key=>[SCALAR]-[ST_INT]-[3],more_key=>[SCALAR]-[ST_INT]-[1]]\n\
hash_9_[HASH]-[key=>[SCALAR]-[ST_INT]-[3]]\n\
list_7_[LIST]-[[SCALAR]-[ST_INT]-[1],[SCALAR]-[ST_INT]-[2],[SCALAR]-[ST_STRING]-[]]\n\
hash_10_[HASH]-[key1=>[SCALAR]-[ST_STRING]-[one],key2=>[SCALAR]-[ST_STRING]-[]]";


	  if (example == GetAsString (RL)) {

	       CPPUNIT_ASSERT (true);
	       return;
	  }

	  //std::cerr << std::endl;
	  //std::cerr << GetAsString (RL);
	  //std::cerr << std::endl;

	  CPPUNIT_ASSERT (false);
	  return;
     }
};
////////////////////////////////////////////////

////////////////////////////////////////////////
CPPUNIT_TEST_SUITE_REGISTRATION (Test_Cobjects);
CPPUNIT_TEST_SUITE_REGISTRATION (Test_Parser);

int main () {

     CppUnit::TextUi::TestRunner runner;
     CppUnit::TestFactoryRegistry &registry =
	  CppUnit::TestFactoryRegistry::getRegistry ();
     runner.addTest ( registry.makeTest () );
     runner.run ();

     return 0;
}
////////////////////////////////////////////////
